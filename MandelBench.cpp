#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include <sys/time.h>


using namespace std;

int pix_real=0;
int pix_imag=0;
int iteration=0;
double z_real=0.0;
double z_imag=0.0;
double w_real=0.0;
double w_imag=0.0;
double c_real=0.0;
double c_imag=0.0;
string line="";
bool bailout=false;

timespec timeSnap;
long timeSnap2;
long timeSnap1;

//timeval timeSnap1;
//timeval timeSnap2;

int maxIterations = 1000000;
long totalIterationCount = 0;

char computeCharacter(int dwell, int maxIts) {
    	if (dwell < (maxIts/4)) {return 'O';}
    	if (((maxIts/4) <= dwell) && (dwell < (maxIts/2))) {return 'o';}
    	if (((maxIts/2) <= dwell) && (dwell < (3*maxIts/4))) {return '.';}
    	if (dwell >= (3*maxIts/4)) {return ' ';}
    	return 'X';
    }
    

int main(int argc, char *argv[]) {
	//timeSnap1 = System.nanoTime();
	//timespec ts;

	clock_gettime(CLOCK_REALTIME, &timeSnap); // Works on Linux
	timeSnap1 = timeSnap.tv_nsec;
	//clock_gettime(CLOCK_MONOTONIC_RAW, &timeSnap); // Works on Linux
	//gettimeofday(&timeSnap1, NULL);
	
	for (pix_imag = 19; pix_imag >= -20; pix_imag--) {
    	c_imag = (double)pix_imag / 20;
    	line = "";
    	for (pix_real = -40; pix_real <= 39; pix_real++) {
    			bailout = false;
    			c_real = (double)pix_real / 40 - .5;
    			z_real = 0.0;
    			z_imag = 0.0;
    			for (iteration = 0; ((iteration < maxIterations) && !bailout); iteration++) {
    				w_real = (z_real * z_real) - (z_imag * z_imag) + c_real;
        			w_imag = (2 * z_real * z_imag) + c_imag;
        			z_real = w_real;
        			z_imag = w_imag;
        			if (z_real * z_real + z_imag * z_imag >= 4) {bailout = true;}
        			totalIterationCount++;
    			}
    			//line = line + computeCharacter(iteration,maxIterations);
    			//why isn't the above working?  Cheap work-around:
    			cout << computeCharacter(iteration,maxIterations);
    		}
    		cout << "\n";
    	}
    	
    	clock_gettime(CLOCK_REALTIME, &timeSnap); // Works on Linux
    	timeSnap2 = timeSnap.tv_nsec;
	//clock_gettime(CLOCK_MONOTONIC, &timeSnap2); // Works on Linux
    	//gettimeofday(&timeSnap2, NULL);
    	
    	cout << "Total iterations calculated: " << totalIterationCount << "\n";
	cout << "Elapsed time: " << ((double)(timeSnap2 - timeSnap1))/1000000 << " seconds.\n";
}


